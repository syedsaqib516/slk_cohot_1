package inheritance;


class A
{

	int x= 10;// no concept of global variables in java
	static int y=20;
	void m1()
	{
		System.out.println("non static method");
	}
	static void m2()
	{
		System.out.println("static method");
	}
	
	

}
class B  extends A
{
	
}

class C extends A
{
	
}
public class Demo1 
{
	public static void main(String[] args) 
	{
        C c = new C();
        c.x=30;  
        c.m1();
        C.y=20;
        C.m2();
	}
}
