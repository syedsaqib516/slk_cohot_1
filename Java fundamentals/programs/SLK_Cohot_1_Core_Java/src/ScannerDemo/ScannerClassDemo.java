package ScannerDemo;
import java.util.Scanner;
public class ScannerClassDemo 
{
	public static void main(String[] args)
	{
       Scanner scan = new Scanner(System.in);
       
       System.out.println("enter s1 string");
      String s1= scan.nextLine();
       System.out.println("enter s2 string");
      String s2= scan.nextLine();
       System.out.println("the concatenation of "+s1+" and "+s2+" is ="+s1+s2);
       
       System.out.println("enter a character");
        char c = scan.next().charAt(0);
        System.out.println(c);
        
//       System.out.println("enter x number");
//      int x= scan.nextInt();
//      System.out.println("enter y number");
//      int y= scan.nextInt();
//      System.out.println("the sum of "+x+" and "+y+" is ="+(x+y));
	}
}
