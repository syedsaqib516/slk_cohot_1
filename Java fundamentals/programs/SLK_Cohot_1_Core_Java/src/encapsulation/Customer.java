package encapsulation;

public class Customer 
{
	private String custId;
	private String custName;
	private String custEmail;
	private	long   custPhone;
	
	

	public Customer(String custId, String custName, String custEmail, long custPhone) {
		super();
		this.custId = custId;
		this.custName = custName;
		this.custEmail = custEmail;
		this.custPhone = custPhone;
	}
	
	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public long getCustPhone() {
		return custPhone;
	}

	public void setCustPhone(long custPhone) {
		this.custPhone = custPhone;
	}

	public String getCustEmail() {
		return custEmail;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}
	
	public String getCustId()
	{
		return this.custId;
	}
	
	public void setCustEmail(String custEmail)
	{
		this.custEmail=custEmail;
	}
	
	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", custName=" + custName + ", custEmail=" + custEmail + ", custPhone="
				+ custPhone + "]";
	}
	
}
