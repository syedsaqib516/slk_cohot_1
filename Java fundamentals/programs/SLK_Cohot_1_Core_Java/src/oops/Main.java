package oops;

class Employee
{
	String empName;// non static vars/instance
	int empId;
	double empSalary;
	static final String COMPANY_NAME="GL";// static var
	
	static void logInOut(int z)// static method
	{
		System.out.println("logged successfully");
		 int x;  // local variable not static/non static
		// System.out.println(x);
		// System.out.println(empName);
	}
	void code()// non static method
	{
		System.out.println("developing software");
		//static int x=10 ; CTE
		System.out.println(COMPANY_NAME);
		
	}
}


public class Main
{
	
	public static void main(String[] args) 
	{
		//
		System.out.println(Employee.COMPANY_NAME);
		Employee.logInOut(0);
		
		Employee emp1 = new Employee();
		System.out.println(emp1.empId);
		System.out.println(emp1.empName);
		
		emp1.empName="syed";
		emp1.empId=134;
		
		Employee emp2 = new Employee();
		emp2.empName="Shreya";
		emp2.empId=123;
		
		
	}

}
