package fileIO;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileReader;
import java.io.FileWriter;

public class FileReaderWriter {
	public static void main(String[] args) throws IOException {
		FileReader fis = new FileReader("input.txt");
		FileWriter fos = new FileWriter("output.txt");
		int temp;
		while((temp=fis.read())!=-1)
		{
			fos.write(temp);
		}

		//fos.flush();
		fos.close();
	}
}
