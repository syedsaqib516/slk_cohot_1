package fileIO;

public class MotorCycle 
{
	String brand;
	String model;
	int cc;
	double price;
	
	public MotorCycle(String brand,String model,int cc,double price)
	{
		this.brand=brand;//shadowing problem
		this.model=model;
		this.cc=cc;
		this.price=price;
	}
	MotorCycle(String brand,String model,int cc)
	{
		this.brand=brand;//shadowing problem
		this.model=model;
		this.cc=cc;
	
	}
	MotorCycle(String brand,String model)
	{
		this.brand=brand;//shadowing problem
		this.model=model;
	
	}
	MotorCycle()
	{
		
	}
	
	
	@Override
	public String toString() {
		return "MotorCycle [brand=" + brand + ", model=" + model + ", cc=" + cc + ", price=" + price + "]";
	}
	

}
