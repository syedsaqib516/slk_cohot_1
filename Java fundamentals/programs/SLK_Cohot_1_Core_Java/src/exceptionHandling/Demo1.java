package exceptionHandling;

import java.util.InputMismatchException;

public class Demo1 extends Exception
{
	public static void main(String[] args) 
	{
		System.out.println("main started");
		int x=10;

		try {
			System.out.println("try start");
			System.out.println(10/0);// new ArithmeticException()
			System.out.println("try end");
		}
		
		catch(ArithmeticException e)//ArithmeticException e = new ArithmeticException()
		{// specialized catch blocks
			System.out.println(e.getMessage());
			System.out.println("please give non 0 denominator");
		}
		catch(InputMismatchException e) {
			System.out.println(e.getMessage());
			System.out.println("please input numbers only");
		}
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
			System.out.println(" please use indexs within range");
		}
		catch(Exception e) // Exception e = new ArithmeticException()
		{// generalized catch blocks
			System.out.println(e);
			System.out.println("something went wrong please try giving valid inputs");
		}

		System.out.println("main ended");
	}

}
