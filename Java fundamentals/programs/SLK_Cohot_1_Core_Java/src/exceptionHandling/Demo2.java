package exceptionHandling;
class Y
{
	
}

class X extends Exception
{
	
}
public class Demo2 
{
	public static void main(String[] args) 
	{
		try {
		 throw new X();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

}
