package multithreading;

import java.util.Iterator;
import java.util.Scanner;

public class Demo2 
{
	public static void main(String[] args) 
	{
		System.out.println("Banking Activity started");
		Scanner sc = new Scanner(System.in);
		System.out.println("enter your acc no");
		sc.next();
		System.out.println("enter your password");
		sc.next();
		System.out.println("enter amount to withdraw");
		sc.next();
		System.out.println("please collect cash....");

		System.out.println("Banking Activity ended");

		System.out.println("Num printing Activity started");
		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			System.out.println(i);

		}
		System.out.println("Num Activity ended");

		System.out.println("char printing Activity started");
		for (int i = 97; i <= 100; i++) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			System.out.println((char)i);
			


		}
		System.out.println("char Activity ended");

	}
}
