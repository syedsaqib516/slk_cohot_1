package multithreading;

public class NumPrinting implements Runnable{
	@Override
	public void run()
	{
		System.out.println("Num printing Activity started");
		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			System.out.println(i);

		}
		System.out.println("Num Activity ended");

	}
}
