package multithreading;

public class MainDemo3{
	public static void main(String[] args)
	{
       BankingActivity bt = new BankingActivity();
       NumPrinting np= new NumPrinting();
       CharPrint cp = new CharPrint();
       
       Thread t1= new Thread(bt);
       Thread t2= new Thread(np);
       Thread t3= new Thread(cp);
      // t1.setPriority(10);
       t2.setPriority(10);
       t3.setPriority(1);
       //t1.start();
       t2.start();
       t3.start();

	}
}
