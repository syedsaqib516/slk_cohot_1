package collection;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

class MyCompartor implements Comparator<String>
{

	@Override
	public int compare(String s1, String s2) {
		return -s1.compareTo(s2);
	}
	
}
public class Demo3 
{
	public static void main(String[] args) {
		LinkedHashSet hs = new LinkedHashSet();
		hs.add(10);
		hs.add(20);
		hs.add("abc");
		hs.add(10);
		System.out.println(hs);
		
		TreeSet<String> ts = new TreeSet<String>(new MyCompartor());
		ts.add("b");
		ts.add("c");
		ts.add("a");
		System.out.println(ts);
		
				
			
				
	}

	
}
