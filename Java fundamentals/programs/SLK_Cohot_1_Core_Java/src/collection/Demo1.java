package collection;

public class Demo1
{
	public static void main(String[] args) 
	{
		int x=10;
		Integer x1=x;// auto wrapping/boxing

		int x2=x1;// auto unboxing

		Integer x3=Integer.valueOf(x);// explicit wraping/boxing
		int x4= x3.intValue();//explicit unwrapping

	}

}
