package collection;

import java.util.TreeMap;

public class Demo4 
{
	public static void main(String[] args) {
		
		TreeMap<Integer,String> hm = new TreeMap<Integer,String>(new MyComparator());
		hm.put(0, "abc");
		hm.put(3, "def");
		hm.put(1, "zyx");
		System.out.println(hm);
		
		
	}

}
