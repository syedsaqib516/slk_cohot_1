package methods;

class Calculator
{
	void dummy()
	{
		System.out.println("dummy method");
	}

	void add(int x ,int y)
	{
		int sum= x+y; 
		System.out.println(" the sum is = "+sum);	
	}
	
	void add(int x ,int y,int z)
	{
		int sum= x+y+z; 
		System.out.println(" the sum is = "+sum);	
	}
	
	
	void add(int x ,double y)
	{
		double sum= x+y; 
		System.out.println(" the sum is = "+sum);	
	}
	void add(double x ,int y)
	{
		double sum= x+y; 
		System.out.println(" the sum is = "+sum);	
	}

	public int sub(int x ,int y)
	{
		int diff= x-y; 
		return	diff;
	}

	public static strictfp double div(double x , double y)// parameters/arguments
	{
		return x/y;
	}

}

public class Main 
{
	public static void main(String[] args) 
	{
		Calculator c = new Calculator();
		
		c.dummy();

		c.add(10, 20);
		
		int result=	c.sub(20, 10);
		
		System.out.println("the diff is = "+result);

		System.out.println("the result is = "+c.div(22,7));


	}
}
