package thisAndSuper;

class A
{
	A()
	{		
		this(10);
		System.out.println("no arg constr");
	}
	A(int x)
	{
		System.out.println("int arg constr");
	}
	A(int y , double d)
	{
		this();
		System.out.println("int and double  arg constr");
	}
}


public class ThisDemo
{
	public static void main(String[] args) {
		new A(10,20.5);
	}

}
