package thisAndSuper;

//public class Object
//{
//	public Object()
//	{
//		
//	}
//}
class X
{
	X(int x)
	{
		
		System.out.println("X no arg constr");
	}
}
class Y extends X
{
	Y()
	{
		super(10);
		System.out.println("Y no arg constr");
	}
}


public class SuperDemo 
{
	public static void main(String[] args) {
		new Y();
	}
}
