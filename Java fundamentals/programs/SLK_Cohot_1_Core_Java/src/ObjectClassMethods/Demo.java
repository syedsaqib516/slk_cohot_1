package ObjectClassMethods;

import java.util.Objects;

class Test
{
	int x ;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Test other = (Test) obj;
		return x == other.x;
	}


	@Override
	public int hashCode() {
		return Objects.hash(x);
	}
	
	
	Test(int x)
	{
		this.x=x;
	}
	@Override
	public String toString() {
		return "Test [x=" + x + "]";
	}	
	
}
public class Demo 
{
	public static void main(String[] args)
	{
		Object obj1= new Object();
		Object obj2= new Object();
		Object obj3=obj2;
		
		System.out.println(obj1.hashCode());
		System.out.println(obj2.hashCode());
		System.out.println(obj1.equals(obj2));
		System.out.println(obj2.equals(obj3));
		String s1="abc";
		String s2 = "abc";
		
		System.out.println(s1.equals(s2));
		
		Test t1= new Test(10);
		System.out.println(t1.getClass().getName());
		System.out.println(s1.getClass().getCanonicalName());
       System.out.println(t1.getClass().getDeclaredMethods());		
		
		
		
		
		
		
		
		
		
//		System.out.println(obj.toString());
//		System.out.println(obj.hashCode());
//		
//		Test test = new Test(10);
//		System.out.println(test.hashCode());
//		
//		String s = "abcd";
//		System.out.println(s.hashCode());
//		System.out.println(System.identityHashCode(s));
		
		
		
		
	}

}
