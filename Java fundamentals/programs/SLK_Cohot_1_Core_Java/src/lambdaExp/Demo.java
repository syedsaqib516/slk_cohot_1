package lambdaExp;

@FunctionalInterface//SAM
interface Itr
{
	public int m1(int n);  
}
public class Demo 
{
	public static void main(String[] args) {
		Itr i = n->n*n;
		System.out.println(i.m1(5));
	}	

}
