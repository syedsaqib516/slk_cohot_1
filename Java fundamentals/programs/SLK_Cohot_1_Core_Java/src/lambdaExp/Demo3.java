package lambdaExp;

import java.util.function.Function;

public class Demo3 
{
	public static void main(String[] args)
	{
		Function<String,String> f= s->s.toUpperCase();
		
		String s= "guidsowjhwbwedowheoiow;dhiwdgue";
		System.out.println(f.apply(s));
		
	}

}
