package lambdaExp;

import java.util.function.Predicate;

public class Demo2 
{
	public static void main(String[] args)
	{
		Predicate<Integer> p1 = n -> n%2==0;

		Predicate<String> p2 = s -> s.length()>5;
		
		String s= "saqib";
		System.out.println(p2.test(s));
		System.out.println(p1.test(18278));
	}

}
