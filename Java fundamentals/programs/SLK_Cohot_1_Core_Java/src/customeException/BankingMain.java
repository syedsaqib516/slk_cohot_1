package customeException;

import java.io.IOException;

public class BankingMain 
{
	public static void main(String[] args) {
//		System.out.println("welcome to SLK Bank");
//		BankTransaction bt = new BankTransaction();
//		bt.validateBalance(20000, 9000);
//		System.out.println("Thank you , please come again");
//		
		try
		{
			try {
				//System.out.println(10/0);
			} catch (NullPointerException e) {
				System.out.println("inner catch handled");
			}
			finally {
				System.out.println("finally executed");
			}
		}
	
		catch (ArithmeticException e) {
			System.out.println("outer catch handled");
		}
	}

}
