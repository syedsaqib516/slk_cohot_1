package customeException;

public class BankTransaction 
{
	public void validateBalance(double amtBal,double amtWithdraw)
	{
		System.out.println("validate balance method started");
		if(amtBal>amtWithdraw)
		{
			System.out.println("withdrawl successful,please collect cash");
			amtBal= amtBal-amtWithdraw;
		}
		else
		{ 
			try {
				throw new InsufficientBalanceException("withdraw within your aukat");
			}

			catch (RuntimeException e) {
				System.out.println(e.getMessage());
			}
		}
		
		System.out.println("Available Balance = "+amtBal);
		System.out.println("validate Balance method ended");
	}


}
