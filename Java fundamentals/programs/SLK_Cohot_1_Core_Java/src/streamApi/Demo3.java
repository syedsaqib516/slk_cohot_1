package streamApi;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class Demo3 
{
   public static void main(String[] args) {
	   ArrayList<Integer> marks = new ArrayList<Integer>();
		marks.add(10);
		marks.add(0);
		marks.add(35);
		marks.add(20);
		marks.add(38);
		marks.add(70);
		marks.add(90);
		System.out.println(marks);
		
		marks.stream().forEach(i->System.out.println(i));

		marks.stream().forEach(System.out::println);
		
		Consumer<Integer> c= i->{
			System.out.println(" the square of the marks "+i+" is = "+(i*i));
		};
		
		marks.stream().forEach(c);
		
		
		Stream s=Stream.of(10,20,30,40,99);
		
		Integer[] i = {1,2,3,4567,7,89,9,990};
		Stream s1=Stream.of(i);



}
}
