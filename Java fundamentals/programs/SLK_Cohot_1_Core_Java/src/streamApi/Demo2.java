package streamApi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Demo2 
{
	public static void main(String[] args) {
		ArrayList<Integer> marks= new ArrayList<Integer>();
		marks.add(5);
		marks.add(6);
		marks.add(7);
		marks.add(8);
		marks.add(9);
		marks.add(1);
		marks.add(2);
		marks.add(3);
		marks.add(4);
		
		
	List<Integer> graceMarksAll=	marks.stream().map(n->n+4).collect(Collectors.toList());
	System.out.println(graceMarksAll);	
	
	List<Integer> graceMarksOnlyFailed=	marks.stream().filter(n->n<5).map(n->n+4).collect(Collectors.toList());
	System.out.println(graceMarksOnlyFailed);
	
	List<Integer> sortedMarksAsc= marks.stream().sorted().collect(Collectors.toList());
	System.out.println(sortedMarksAsc);
	
	List<Integer> sortedMarksDsc= marks.stream().sorted((i1,i2)->-i1.compareTo(i2)).collect(Collectors.toList());
	System.out.println(sortedMarksDsc);
	
	
	Long numOfFailed=marks.stream().filter(n->n<5).count();
	System.out.println(numOfFailed);
	

	
	
	
	
		
	}

}
