package polymorphism;

class A
{
	void m1()
	{
		System.out.println("m1 inherited");
	}
	void m2()
	{
		System.out.println("m2 ");
	}

}
class B extends A
{
	void m2()
	{
		System.out.println("m2 changed/overidden B ");
	}
	void m3()
	{
		System.out.println("m3 specialized B");
	}
}
class C extends A
{
	void m2()
	{
		System.out.println("m2 changed/overidden C ");
	}
	void m3()
	{
		System.out.println("m3 specialized c");
	}
}


public class Demo3
{
	public static void main(String[] args) 
	{
		//		A a = new A(); // tight coupling 
		//		  a.m2();
		//		B b = new B();
		//		  b.m2();
		//		C c = new C();
		//		  c.m2();

		A a = new B();// lose coupling/upcasting
		a.m1();// inherited
		a.m2();// overidden
		B b =(B)a;// downcasting
		b.m3();
		((B)(a)).m3();// down casting for speacialized method
		a = new C();
		a.m1();
		a.m2();
		((C)(a)).m3();


	}

}
