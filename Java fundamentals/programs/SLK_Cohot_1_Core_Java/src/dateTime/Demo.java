package dateTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Demo 
{
	public static void main(String[] args) 
	{
		LocalDate ld = LocalDate.now();
		System.out.println(ld);
		
		LocalDate birthDate= LocalDate.of(1996, 04, 19);
		System.out.println(birthDate);
		
		int yy=ld.getYear();
		int mm=ld.getMonthValue();
		int dd=ld.getDayOfMonth();
		System.out.printf("%d/%d/%d",dd,mm,yy);
		System.out.println();
		
		LocalTime lt = LocalTime.now();
	   System.out.println(lt);
	   
	   LocalDateTime ldt = LocalDateTime.now();
	   System.out.println(ldt);
	   
	   Period p = Period.between(ld, birthDate);
	   System.out.println(p);
	   System.out.println(p.getYears());
	   
	   
	   ZoneId zi= ZoneId.of("America/Los_Angeles");
	   ZonedDateTime zdt = ZonedDateTime.now(zi);
	     
	   System.out.println(zdt);
		
	}

}
